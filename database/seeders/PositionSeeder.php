<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert($this->getData());
    }

    private function getData(): array
    {
        return [
            [
                'name' => 'Кандидат',
                'salary' => '0'
            ],
            [
                'name' => 'Менеджер компьютера',
                'salary' => '60000'
            ],
            [
                'name' => 'Разработчик младший',
                'salary' => '80000'
            ],
            [
                'name' => 'Разработчик старший',
                'salary' => '1500000'
            ],
            [
                'name' => 'Разработчик средний',
                'salary' => '1000000'
            ],
            [
                'name' => 'Лидер разработчиков',
                'salary' => '2000000'
            ],
            [
                'name' => 'Директор',
                'salary' => '10000000'
            ],
        ];
    }
}
