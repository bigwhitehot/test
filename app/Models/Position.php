<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Position extends Model
{
    /**
     * @return HasMany
     */
    public function employers(): HasMany
    {
        return $this->hasMany(Employer::class);
    }
}
